// https://spin.atomicobject.com/2017/04/20/typesafe-container-components/

import * as React from "react";
import { Player } from "../logic/player";
import { Color } from "../colors";
import { Move } from "../logic/move";
import { connect } from "react-redux";
import "./styles.css";
import { makeMove, reMove } from "../redux/types";

// commit 1
// commit 3
interface ButtonProps {
  player: Player;
  row: number;
  col: number;
  makeMove: (move: Move) => void;
  reMove: () => void;
}
interface ButtonState {}

class Button extends React.Component<ButtonProps, ButtonState> {
  onClickButton(click: boolean) {
    if (click) {
      this.props.makeMove(new Move(this.props.row, this.props.col));
      // this.props.reMove();
    }
  }

  render() {
    let clickAccepted = false;
    let color = this.props.player && this.props.player.color;
    let name = this.props.player && this.props.player.name;
    if (color === Color.GREY) {
      clickAccepted = true;
    }
    return (
      <div>
        <button className={color} onClick={() => this.onClickButton(clickAccepted)}>
          {name}
        </button>
      </div>
    );
  }
}

interface StateFromProps {}

interface DispatchFromProps {
  makeMove: (move: Move) => void;
  reMove: () => void;
}

const makeMove2 = (dispatch: any) => (move: Move) => {
  dispatch(makeMove({ move: move }));
};
const reMove2 = (dispatch: any) => () => {
  dispatch(reMove());
};

const mapDispatchToProps = (dispatch: any): DispatchFromProps => {
  return {
    makeMove: makeMove2(dispatch),
    reMove: reMove2(dispatch)
  };
};

export default connect<StateFromProps, DispatchFromProps>(
  null,
  mapDispatchToProps
)(Button);
