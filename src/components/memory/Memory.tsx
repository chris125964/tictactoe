import _ from "lodash";
import * as React from "react";
import { connect } from "react-redux";
import Tile from "./tile";
import "./styles.css";

interface MemoryProps {
  order: string[];
}
interface MemoryState {}

function doubleImages(images: string[]) {
  let doubleImages: string[] = [];
  images.forEach(image => {
    doubleImages.push(image);
    doubleImages.push(image);
  });
  return doubleImages;
}

function shuffleImages(images: string[]) {
  for (let i = images.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = images[i];
    images[i] = images[j];
    images[j] = temp;
  }
}

function renderTiles(images: string[]) {
  let doubleArray: string[] = doubleImages(images);
  shuffleImages(doubleArray);
  return _.map(doubleArray, imageString => <Tile show={false} person={imageString} />);
}

export class Memory extends React.Component<MemoryProps, MemoryState> {
  render() {
    return <div className="wrapper">{renderTiles(this.props.order)}</div>;
  }
}

interface StateFromProps {}

// commit 3
interface DispatchFromProps {}

const mapStateToProps = (state: any): StateFromProps => {
  return {
    thisGame: state.tictactoe.thisGame,
    nrMoves: state.tictactoe.nrMoves
  };
};

const mapDispatchToProps = (dispatch: any): DispatchFromProps => {
  return {};
};

export default connect<StateFromProps, DispatchFromProps>(
  mapStateToProps,
  mapDispatchToProps
)(Memory);
