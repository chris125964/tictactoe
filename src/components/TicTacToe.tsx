import * as React from "react";
import { ButtonRow } from "./buttonRow";
import { connect } from "react-redux";
import { Game } from "../logic/game";
import "./styles.css";

interface TicTacToeProps {
  thisGame: Game;
  nrMoves: number;
}
interface TicTacToeState {}

export class TicTacToe extends React.Component<TicTacToeProps, TicTacToeState> {
  getRow(game: Game, row: number) {
    let row0Array = [];
    for (var colLoop = 0; colLoop < game._nrCols; colLoop++) {
      row0Array.push(game.get(row, colLoop));
    }
    return row0Array;
  }

  render() {
    let game = this.props.thisGame;
    let gameOverButton = game && game.hasGameWon(game.Players.get(0)) ? <button>Game over</button> : <div />;
    let row0Array = this.getRow(game, 0);
    let row1Array = this.getRow(game, 1);
    let row2Array = this.getRow(game, 2);
    let move = this.props.thisGame.bestMove(this.props.thisGame.whoseTurnIsIt(), 0, false);

    return (
      <div>
        <div className="container1">
          <h2>Züge: {this.props.nrMoves}</h2>
          <h2>Dran ist: {this.props.thisGame.whoseTurnIsIt()._name}</h2>
        </div>
        <h2>
          Bester Zug: {move.Row} - {move.Col}
        </h2>
        <div className="container2">
          <ButtonRow value={row0Array} row={0} />
          <ButtonRow value={row1Array} row={1} />
          <ButtonRow value={row2Array} row={2} />
        </div>
        <div className="container">{gameOverButton}</div>
      </div>
    );
  }
}

interface StateFromProps {
  thisGame: Game;
  nrMoves: number;
}

// commit 3
interface DispatchFromProps {}

const mapStateToProps = (state: any): StateFromProps => {
  return {
    thisGame: state.tictactoe.thisGame,
    nrMoves: state.tictactoe.nrMoves
  };
};

const mapDispatchToProps = (dispatch: any): DispatchFromProps => {
  return {};
};

export default connect<StateFromProps, DispatchFromProps>(
  mapStateToProps,
  mapDispatchToProps
)(TicTacToe);
