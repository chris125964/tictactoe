export class Fields {
  _row: number;
  _col: number;
  _map: Map<string, { row: number; col: number; obj: any }>;

  constructor(row: number, col: number, defaultObject: any) {
    this._row = row;
    this._col = col;
    this._map = new Map();
    for (var rowLoop = 0; rowLoop < row; rowLoop++) {
      for (var colLoop = 0; colLoop < col; colLoop++) {
        let ob = rowLoop + "---" + colLoop;
        this._map.set(ob, {
          row: rowLoop,
          col: colLoop,
          obj: defaultObject
        });
      }
    }
  }

  get(row: number, col: number): { row: number; col: number; obj: any } {
    let ob = row + "---" + col;
    let field = this._map.get(ob);
    if (field === undefined) {
      field = {
        row,
        col,
        obj: ""
      };
    }
    return field;
  }

  set(row: number, col: number, obj: any): any {
    let ob = row + "---" + col;
    this._map.set(ob, {
      row,
      col,
      obj
    });
  }

  clone(): Fields {
    let cloneFields = new Fields(
      this._row,
      this._col,
      this.get(this._row, this._col).obj
    );
    this.each(field => cloneFields.set(field.row, field.col, field.obj));
    return cloneFields;
  }

  each(callback: (result: { row: number; col: number; obj: any }) => any) {
    this._map.forEach((value, key) => {
      callback(value);
    });
  }
}
