import { PlayerList } from "../playerlist";
import { Player } from "../player";
import { Color } from "../../colors";

describe("PlayerList", () => {
  let player1, player2, player3;
  let pl;
  beforeEach(() => {
    player1 = new Player("Spieler 1", Color.RED);
    player2 = new Player("Spieler 2", Color.BLUE);
    player3 = new Player("Spieler 3", Color.GREEN);
    pl = new PlayerList([player1, player2, player3]);
  });

  it("constructor", () => {
    expect(pl.whoseTurnIsIt()).toBe(player1);

    expect(pl.next()).toBe(player2);
    expect(pl.next()).toBe(player3);
    expect(pl.next()).toBe(player1);
  });

  it("get", () => {
    expect(pl.get(0)).toBe(player1);
  });

  it("setTurnOn", () => {
    pl.setTurnOn(player2);
    expect(pl.whoseTurnIsIt()).toBe(player2);
  });

  it("next", () => {});

  it("clone", () => {
    pl.next();
    let clonePlayerList = pl.clone();
    pl.next();
    expect(clonePlayerList.get(0)).toBe(player1);
    expect(clonePlayerList.get(1)).toBe(player2);
    expect(clonePlayerList.get(2)).toBe(player3);
    expect(clonePlayerList.whoseTurnIsIt()).toBe(player2);
    expect(pl.whoseTurnIsIt()).toBe(player3);
  });

  it("each", () => {
    pl.each((player, index) => expect(player.name).toBe("Spieler " + (index + 1)));
  });
});
