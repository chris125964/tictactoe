import _ from "lodash";

export class Move {
  _moveNr: number;
  _row: number;
  _col: number;
  _rating: number;
  _finished: boolean;

  constructor(row: number, col: number, moveNr: number = 0.0, rating: number = 0.0, finished = false) {
    this._row = row;
    this._col = col;
    this._moveNr = moveNr;
    this._rating = rating;
    this._finished = finished;
  }

  /** kopiert einen Move
   * @returns Move
   */
  clone(): Move {
    return _.cloneDeep(this);
  }

  get MoveNr(): number {
    return this._moveNr;
  }

  set MoveNr(moveNr: number) {
    this._moveNr = moveNr;
  }

  get Row(): number {
    return this._row;
  }

  set Row(row: number) {
    this._row = row;
  }

  get Col(): number {
    return this._col;
  }

  set Col(col: number) {
    this._col = col;
  }

  get Rating(): number {
    return this._rating;
  }
  set Rating(rating: number) {
    this._rating = rating;
  }

  get WholeRating(): number {
    return this._rating + (this._finished ? 1 : 0);
  }

  get Finished(): boolean {
    return this._finished;
  }
  set Finished(finished: boolean) {
    this._finished = finished;
  }
}
