import _ from "lodash";
// import { Move } from "./draw";
import { Player } from "./player";
import { PlayerList } from "./playerlist";
import { Color } from "../colors";
import { Fields } from "./fields";
import { Move } from "./move";
import { Moves } from "./moves";

export const WIN_RATING: number = 100.0;
export const LOOSE_RATING: number = -100.0;

export interface IGame {
  makeMove: (move: Move) => void;
  switchTurn: () => Player;
  whoseTurnIsIt: () => Player;
  setTurn: (player: Player) => void;
  hasGameWon: (player: Player) => boolean;
}

export class Game implements IGame {
  initValue: string;
  _nrRows: number;
  _nrCols: number;
  _players: PlayerList;
  _turnPlayer1: boolean;
  _fields: Fields;

  constructor(nrRows: number, nrCols: number, players: PlayerList, initValue: string) {
    this.initValue = initValue;
    this._nrRows = nrRows;
    this._nrCols = nrCols;
    this._players = players;
    let playerUndefined = new Player(initValue, Color.GREY);
    this._turnPlayer1 = true;
    this._fields = new Fields(nrRows, nrCols, playerUndefined);
    for (var rowLoop = 0; rowLoop < this.Row; rowLoop++) {
      let row = new Array<Player>();
      for (var colLoop = 0; colLoop < this.Col; colLoop++) {
        row.push(playerUndefined);
      }
    }
  }

  get Row(): number {
    return this._nrRows;
  }
  get Col(): number {
    return this._nrCols;
  }
  get Players(): PlayerList {
    return this._players;
  }

  setTurn(player: Player): void {
    this._turnPlayer1 = player === this._players.whoseTurnIsIt();
  }

  switchTurn(): Player {
    this._turnPlayer1 = !this._turnPlayer1;
    return this.Players.next();
  }

  /** prüft, welcher Spieler an der Reihe ist
   * @returns der Spieler, der an der Reihe ist
   */
  whoseTurnIsIt(): Player {
    let pl: PlayerList = this.Players;
    return pl.whoseTurnIsIt();
  }

  // eachRow(callbackfn: (value: Array<Player>, index: number) => void): void {
  //   for (var rowLoop = 0; rowLoop < this._nrRows; rowLoop++) {
  //     let colArray = [];
  //     for (var colLoop = 0; colLoop < this._nrCols; colLoop++) {
  //       let field = this._fields.get(rowLoop, colLoop);
  //       if (field && field.obj) {
  //         colArray.push(field.obj.name);
  //       }
  //     }
  //     // console.log(`${rowLoop}: ${colArray[0]} ${colArray[1]} ${colArray[2]}`);
  //   }
  // }

  /** kopiert ein Game
   * @returns Game
   */
  clone(): Game {
    return _.cloneDeep(this);
  }

  makeMove(move: Move): void {
    this.set(move.Row, move.Col);
  }
  set(row: number, col: number): void {
    if (this.whoseTurnIsIt() === this._players.get(0)) {
      this._fields.set(row, col, this._players.get(0));
    } else {
      this._fields.set(row, col, this._players.get(1));
    }
    //this.switchTurn();
  }
  get(row: number, col: number): Player {
    return this._fields.get(row, col).obj;
  }

  isFieldPartOfWinningSituation(
    row: number,
    col: number,
    player: Player,
    rowDirection: number,
    colDirection: number
  ): boolean {
    let gameWon = false;
    let field1 = this._fields.get(row + rowDirection, col + colDirection);
    if (field1.obj.name === player.name) {
      let y2 = this._fields.get(row + 2 * rowDirection, col + 2 * colDirection);
      if (y2.obj.name === player.name) {
        gameWon = true;
      }
    }
    return gameWon;
  }

  hasGameWon(player: Player): boolean {
    let gameWon = false;
    let winningSituation = false;
    this._fields.each(field => {
      if (field.obj && field.obj.name === player.name) {
        let row = field.row;
        let col = field.col;
        winningSituation =
          this.isFieldPartOfWinningSituation(row, col, player, 0, 1) ||
          this.isFieldPartOfWinningSituation(row, col, player, 1, 0) ||
          this.isFieldPartOfWinningSituation(row, col, player, 1, 1) ||
          this.isFieldPartOfWinningSituation(row, col, player, 1, -1);
      }
      gameWon = gameWon || winningSituation;
    });
    return gameWon;
  }

  /** liefert einen zufälligen Zug zurück
   * @returns Move der gefundene Zug
   */
  getRandomMove(): Move {
    let arr = this.getAllPossibleMoves();
    let move = new Move(-1, -1);
    if (arr.length > 0) {
      for (var arrLoop = 0; arrLoop < arr.length; arrLoop++) {
        // console.log(`possible move: [${drw.row}][${drw.col}]`);
      }
      let randomIndex = Math.floor(Math.random() * arr.length);
      move.Row = arr[randomIndex].Row;
      move.Col = arr[randomIndex].Col;
      // console.log(`found -> (${move.Row}, ${move.Col})`);
    } else {
      console.log(`kein Zug mehr`);
    }
    return move;
  }

  getAllPossibleMoves(): Array<Move> {
    let arrayOfMoves = new Array<Move>();
    for (var rowLoop = 0; rowLoop < this.Row; rowLoop++) {
      for (var colLoop = 0; colLoop < this.Col; colLoop++) {
        let player: Player = this.get(rowLoop, colLoop);
        if (player && player.name === this.initValue) {
          arrayOfMoves.push(new Move(rowLoop, colLoop));
        }
      }
    }
    return arrayOfMoves;
  }

  /**
   * ermittelt alle möglichen Züge und bewertet diese folgendermassen:
   * - Spieler 1 hat gewonnen => 1.0
   * - Spieler 2 hat gewonnen => -1.0
   * - andernfalls => 0.0
   * @param player1 Spieler 1
   * @param player2 Spieler 2
   * @return Liste aller möglichen Züge
   */
  // getMoveRating(player1: Player, player2: Player): Move[] {
  //   let moves: Move[] = this.getAllPossibleMoves();
  //   moves.forEach(move => {
  //     let cloneGame = this.clone();
  //     cloneGame.makeMove(move);
  //     cloneGame.switchTurn();
  //     if (cloneGame.hasGameWon(player1)) {
  //       move.Rating = WIN_RATING;
  //     } else if (cloneGame.hasGameWon(player2)) {
  //       move.Rating = LOOSE_RATING;
  //     } else {
  //       move.Rating = 0.0;
  //     }
  //   });
  //   return moves;
  // }

  /**
   * ermittelt den besten Zug für einen Spieler in einer bestimmten Spielsituation.
   * Der Algorithmus orientiert sich dabei am MinMax-Algorithmus (https://de.wikipedia.org/wiki/Minimax-Algorithmus)
   *  bzw. an der Alpha-Beta-Suche (https://de.wikipedia.org/wiki/Alpha-Beta-Suche)
   * @param playerToRate der Spieler, für den der beste Zug gesucht werden soll
   * @param moveNr die aktuelle Zugnummer (beginnt bei 0, kann auf Grund von Rekursion steigen)
   * @param cutOff soll die Suche abgebrochen werden, wenn kein besserer Zug mehr gefunden werden kann?
   * @param print soll der Entscheidungsbaum ausgegeben werden?
   * @return der nach MinMax beste Zug
   */
  bestMove(playerToRate: Player, moveNr: number, cutOff: boolean = true, print: boolean = false): Move {
    let gameFinished = false;
    let bestMove: Move | undefined;

    let moves: Move[] = this.getAllPossibleMoves();
    if (moves.length > 0) {
      // es sind noch Züge möglich
      let currentMoves: Moves = new Moves(_.isEqual(playerToRate, this.whoseTurnIsIt()));
      moves.some(currentMove => {
        let foundFinish: boolean = false;
        // noch kein Spielende => führe den aktuellen Zug aus und prüfe weiter
        let clonedGame: Game = this.clone();
        let currentPlayer: Player = clonedGame.whoseTurnIsIt();
        clonedGame.makeMove(currentMove);
        clonedGame.switchTurn();
        let newMove: Move;
        if (clonedGame.hasGameWon(currentPlayer)) {
          // irgendein Spieler hat das Spiel gewonnen
          newMove = new Move(
            currentMove.Row,
            currentMove.Col,
            moveNr,
            _.isEqual(currentPlayer, playerToRate) ? WIN_RATING : LOOSE_RATING,
            true
          );
          gameFinished = true;
          foundFinish = cutOff;
        } else {
          // das Spiel ist noch nicht zu Ende
          let bestMove: Move = clonedGame.bestMove(playerToRate, moveNr + 1, cutOff, print);
          newMove = new Move(currentMove.Row, currentMove.Col, moveNr, bestMove.Rating);
        }
        if (print) {
          this.printMove(currentPlayer, moveNr, newMove, gameFinished ? "(finished)" : "");
        }
        currentMoves.add(newMove);
        return foundFinish;
      });
      // currentMoves.display(moveNr);

      bestMove = currentMoves.summarizeBestMove();
    } else {
      // es gibt keine möglichen Züge mehr => unentschieden
      gameFinished = true;
      bestMove = new Move(-1, -1, 0, 0, gameFinished);
    }
    if (moveNr === 0 && print) {
      console.log(`======================== decision tree end ========================`);
    }
    return new Move(bestMove.Row, bestMove.Col, bestMove.MoveNr, bestMove.Rating, gameFinished);
  }

  /**
   *
   * @param text
   */
  // display(text: string): void {
  //   console.log(`${text}`);
  //   this.eachRow((row, rowIndex) => {
  //     console.log(`${rowIndex}: ${row[0].name} ${row[1].name} ${row[2].name}`);
  //   });
  // }

  printMove(currentPlayer: Player, moveNr: number, newMove: Move, fin: string): void {
    let topPrefix: string = "";
    let mainPrefix: string = "";
    let bottomPrefix: string = "";
    for (let loop = 0; loop < moveNr; loop++) {
      if (loop === moveNr - 1) {
        topPrefix += "        ";
        mainPrefix += "+------ ";
        bottomPrefix += "|       ";
      } else {
        topPrefix += "        ";
        mainPrefix += "        ";
        bottomPrefix += "        ";
      }
    }
    for (let rowLoop = 0; rowLoop < 3; rowLoop++) {
      let line: string = "";
      if (rowLoop === 0) {
        line += topPrefix + "| ";
      } else if (rowLoop === 1) {
        line += mainPrefix + moveNr + ")";
      } else {
        line += bottomPrefix + "  ";
      }
      line += "  ";
      // jetzt die Spielsteine
      for (let columnLoop = 0; columnLoop < 3; columnLoop++) {
        if (rowLoop === newMove.Row && columnLoop === newMove.Col) {
          line += "(" + currentPlayer.name + ")";
        } else {
          line += " " + this.get(rowLoop, columnLoop).name + " ";
        }
      }
      if (rowLoop === 1) {
        line +=
          "        [" +
          newMove.Row +
          "-" +
          newMove.Col +
          "] -> " +
          newMove.Rating +
          "  " +
          fin +
          " (player " +
          currentPlayer.name +
          ")";
      }
      console.log(line);
    }
  }
}
