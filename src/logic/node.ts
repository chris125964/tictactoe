import _ from "lodash";

export class Node {
  _value: String;
  _children: Node[];
  _parent: Node;

  constructor(val: any) {
    this._value = val;
    this._children = [];
    this._parent = null;
  }

  getValue(): any {
    return this._value;
  }
  setValue(value: any): void {
    this._value = value;
  }
  setParent(node: Node): void {
    this._parent = node;
  }
  getParent(): Node {
    return this._parent;
  }
  addChild(node: Node): void {
    node.setParent(this);
    this._children[this._children.length] = node;
  }
  getNrChildren(): number {
    return this._children.length;
  }
  getChild(index: number): Node {
    return this._children[index];
  }
  getChildren(): Node[] {
    return this._children;
  }
  removeChildren(): void {
    this._children = [];
  }
  traverse(
    node: Node,
    depth: number,
    currentResult: object,
    visitBefore: (node: Node, currentResult: object) => object,
    visitAfter: (node: Node, currentResult: object) => object
  ): object {
    let children: Node[] = node.getChildren();
    let currentObjectBefore: object = currentResult;
    let currentObject: object;
    if (visitBefore !== null) {
      currentObjectBefore = visitBefore(node, currentResult);
    }
    children.forEach(child => {
      currentObjectBefore = this.traverse(child, depth + 1, currentObjectBefore, visitBefore, visitAfter);
    });
    let currentObjectAfter: object = currentObjectBefore;
    if (visitAfter !== null) {
      currentObjectAfter = visitAfter(node, currentObjectBefore);
    }
    return currentObjectAfter;
  }

  trav(
    node: Node,
    depth: number,
    currentResult: object,
    visitBefore: (node: Node, currentResult: object) => object,
    visitAfter: (node: Node, currentResult: object) => object
  ): object {
    let currentNode: any = {};
    currentNode.value = node.getValue();
    let children: Node[] = node.getChildren();
    children.forEach(child => {
      let currentChild: any = this.trav(child, depth + 1, currentResult, visitBefore, visitAfter);
      if (visitAfter !== null) {
        currentNode = visitAfter(currentChild, currentNode);
      }
    });
    return currentNode;
  }
}

// function Node(value) {

//   this.value = value;
//   this.children = [];
//   this.parent = null;

//   this.setParentNode = function(node) {
//       this.parent = node;
//   }

//   this.getParentNode = function() {
//       return this.parent;
//   }

//   this.addChild = function(node) {
//       node.setParentNode(this);
//       this.children[this.children.length] = node;
//   }

//   this.getChildren = function() {
//       return this.children;
//   }

//   this.removeChildren = function() {
//       this.children = [];
//   }
// }

// var root = new Node('root');
// root.addChild(new Node('child 0'));
// root.addChild(new Node('child 1'));
// var children = root.getChildren();
// for(var i = 0; i < children.length; i++) {
//   for(var j = 0; j < 5; j++) {
//       children[i].addChild(new Node('second level child ' + j));
//   }
// }
// console.log(root);
// children[0].removeChildren();
// console.log(root);
// console.log(root.getParentNode());
// console.log(children[1].getParentNode());
