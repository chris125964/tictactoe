import { Node } from "../node";

describe("Tree", () => {
  let parentNode;

  beforeEach(() => {
    parentNode = new Node("root");
  });

  it("node - parent", () => {
    let parentNode = new Node();
    parentNode.setParent(parentNode);
    expect(parentNode.getValue("root"));
    expect(parentNode.getParent()).toBe(parentNode);
  });

  it("node - addChild", () => {
    parentNode.addChild(new Node("child"));
    let children = parentNode.getChildren();
    expect(children.length).toBe(1);
    expect(children[0].getValue()).toBe("child");
  });

  it("node - getChildren", () => {
    parentNode.addChild(new Node("child1"));
    parentNode.addChild(new Node("child2"));
    let children = parentNode.getChildren();
    expect(children.length).toBe(2);
    expect(children[0].getValue()).toBe("child1");
    expect(children[1].getValue()).toBe("child2");
  });

  it("node - removeChildren", () => {
    parentNode.addChild(new Node("child1"));
    parentNode.addChild(new Node("child2"));
    let children = parentNode.getChildren();
    expect(children.length).toBe(2);
    expect(children[0].getValue()).toBe("child1");
    expect(children[1].getValue()).toBe("child2");
    parentNode.removeChildren();
    expect(parentNode.getChildren()).toStrictEqual([]);
  });

  it("node - getChild", () => {
    function visitNode(node, currentResult) {
      let result;
      if (currentResult === null) {
        result = [];
      } else {
        result = currentResult;
      }
      if (result !== null) {
        result.push(node._value);
      }
      return result;
    }
    let child1 = new Node(1);
    let child2 = new Node(2);
    let child3 = new Node(3);
    let child4 = new Node(4);
    child2.addChild(child3);

    parentNode.addChild(child1);
    parentNode.addChild(child2);
    parentNode.addChild(child4);
    expect(parentNode.getChild(0).getValue()).toBe(1);
    expect(parentNode.getChild(1).getValue()).toBe(2);
    expect(parentNode.getChild(2).getValue()).toBe(4);
    expect(
      parentNode
        .getChild(1)
        .getChild(0)
        .getValue()
    ).toBe(3);

    let currentObjectAfter = parentNode.traverse(parentNode, 0, null, null, visitNode);
    expect(currentObjectAfter).toStrictEqual([1, 3, 2, 4, "root"]);
    let currentObjectBefore = parentNode.traverse(parentNode, 0, null, visitNode, null);
    expect(currentObjectBefore).toStrictEqual(["root", 1, 2, 3, 4]);
  });

  it("node - trav", () => {
    function visitNode(node, currentResult) {
      let result = currentResult;
      let childString = "children";
      if (!(childString in result)) {
        result[childString] = [];
      }
      result[childString].push(node);
      return result;
    }
    let child1 = new Node(1);
    let child2 = new Node(2);
    let child3 = new Node(3);
    let child4 = new Node(4);
    child2.addChild(child3);

    parentNode.addChild(child1);
    parentNode.addChild(child2);
    parentNode.addChild(child4);
    expect(parentNode.getChild(0).getValue()).toBe(1);
    expect(parentNode.getChild(1).getValue()).toBe(2);
    expect(parentNode.getChild(2).getValue()).toBe(4);
    expect(
      parentNode
        .getChild(1)
        .getChild(0)
        .getValue()
    ).toBe(3);

    let currentObjectAfter = parentNode.trav(parentNode, 0, null, null, visitNode);
    expect(currentObjectAfter).toStrictEqual({
      children: [
        { value: 1 },
        {
          value: 2,
          children: [{ value: 3 }]
        },
        { value: 4 }
      ],
      value: "root"
    });
  });
});
