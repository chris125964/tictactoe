import { Player } from "../player";
import { Color } from "../../colors";

describe("Player", () => {
  let player;

  beforeEach(() => {
    player = new Player("player", Color.BLUE);
  });

  it("name - getter", () => {
    let player = new Player("user", Color.RED);
    expect(player.name).toBe("user");
  });
  it("color - getter", () => {
    let player = new Player("user", "red");
    expect(player.color).toBe("red");
  });
  it("clone", () => {
    let clonePlayer = player.clone();
    expect(clonePlayer.name).toBe(player.name);
    expect(clonePlayer.color).toBe(player.color);
  });
});
