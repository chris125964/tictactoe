import { TicTacToeState, initGame, makeMove, reMove, pong } from "./types";
import { combineReducers } from "redux";
import { createReducer } from "redux-starter-kit";
import { Game } from "../logic/game";
import { Player } from "../logic/player";
import { Color } from "../colors";
import { PlayerList } from "../logic/playerlist";
import { Move } from "../logic/move";
// import { Move } from "../logic/move";

let player1: Player = new Player("1", Color.RED);
let player2: Player = new Player("2", Color.GREEN);
let playerList = new PlayerList([player1, player2]);
const initialState: TicTacToeState = {
  thisGame: new Game(3, 3, playerList, "x"),
  nrMoves: 0
};
initialState.thisGame.makeMove(new Move(0, 0));
initialState.thisGame.switchTurn();
initialState.thisGame.makeMove(new Move(0, 1));
initialState.thisGame.switchTurn();
initialState.thisGame.makeMove(new Move(1, 0));
initialState.thisGame.switchTurn();
initialState.thisGame.makeMove(new Move(2, 0));
initialState.thisGame.switchTurn();
initialState.thisGame.makeMove(new Move(0, 2));
initialState.thisGame.switchTurn();

export const ticTacToeReducer = createReducer(initialState, {
  [initGame.type]: (state, action) => {
    let newState = {
      thisGame: new Game(3, 3, playerList, "x"),
      nrMoves: 0
    };
    return newState;
  },
  [makeMove.type]: (state, action) => {
    state.thisGame.makeMove(action.payload.move);
    state.thisGame.switchTurn();
    let newState = {
      thisGame: state.thisGame.clone(),
      nrMoves: state.nrMoves
    };
    return newState;
  },
  [reMove.type]: (state, action) => {
    // Gegenzug
    let bestMove: Move = state.thisGame.bestMove(state.thisGame.whoseTurnIsIt(), 0, false, true);
    if (!bestMove) {
      bestMove = state.thisGame.getRandomMove();
    }
    // let draw = state.thisGame.getRandomMove();
    let newState;
    if (bestMove.Row !== -1) {
      state.thisGame.makeMove(bestMove);
      state.thisGame.switchTurn();
      newState = {
        thisGame: state.thisGame.clone(),
        nrMoves: state.nrMoves
      };
    }
    return newState;
  },
  [pong.type]: (state, action) => {
    // Pong
    let newState = {
      thisGame: state.thisGame.clone(),
      nrMoves: state.nrMoves + 1
    };
    return newState;
  }
});

export const rootReducer = combineReducers({
  tictactoe: ticTacToeReducer
});
