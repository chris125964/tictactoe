import * as React from "react";
import Button from "./button";
import { Player } from "../logic/player";
import "./styles.css";

// commit 2
// commit 3
interface ButtonRowProps {
  value: Array<Player>;
  row: number;
}
interface ButtonRowState {}

export class ButtonRow extends React.Component<ButtonRowProps, ButtonRowState> {
  render() {
    let player0 = this.props.value && this.props.value[0];
    let player1 = this.props.value && this.props.value[1];
    let player2 = this.props.value && this.props.value[2];
    return (
      <div className="buttonRow">
        <Button player={player0} row={this.props.row} col={0} />
        <Button player={player1} row={this.props.row} col={1} />
        <Button player={player2} row={this.props.row} col={2} />
      </div>
    );
  }
}
