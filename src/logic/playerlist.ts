import _ from "lodash";
import { Player } from "./player";

export class PlayerList {
  private _list: Array<Player>;
  private _turnIndex: number;

  constructor(players: Array<Player>) {
    this._list = [];

    players.forEach(player => this._list.push(player));
    this._turnIndex = 0;
  }

  clone(): PlayerList {
    let pl = new PlayerList(this._list);
    pl._turnIndex = this._turnIndex;
    return pl;
  }

  whoseTurnIsIt(): Player {
    return this._list[this._turnIndex];
  }

  next(): Player {
    ++this._turnIndex;
    if (this._turnIndex >= this._list.length) {
      this._turnIndex = 0;
    }
    // this.displayList();
    return this.whoseTurnIsIt();
  }

  setTurnOn(playerToTurn: Player): void {
    this._turnIndex = _.findIndex(this._list, player => player === playerToTurn);
  }

  get(index: number): Player {
    return this._list[index];
  }

  each(callbackfn: (player: Player, index: number) => void): void {
    this._list.forEach((player, index) => callbackfn(player, index));
  }
}
