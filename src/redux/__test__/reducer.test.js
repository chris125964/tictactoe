import { Game } from "../../logic/game";
import { PlayerList } from "../../logic/playerlist";
import { Player } from "../../logic/player";
import { Color } from "../../colors";
import { Move } from "../../logic/move";
import { ticTacToeReducer } from "../reducers";

describe("Reducer", () => {
  let player1, player2;
  let playerList;
  let game;
  let initState;

  beforeEach(() => {
    player1 = new Player("1", Color.RED);
    player2 = new Player("2", Color.GREEN);
    playerList = new PlayerList([player1, player2]);
    game = new Game(3, 3, playerList, "x");
    initState = {
      thisGame: game,
      nrMoves: 0
    };
  });

  it("make move", () => {
    const move = new Move(0, 0, 0, 0);
    const action = {
      payload: {
        move
      },
      type: "MAKE_DRAW"
    };
    let expectedGame = game.clone();
    expectedGame.makeMove(move);
    expectedGame.switchTurn();
    const expectedState = {
      nrMoves: 0,
      thisGame: expectedGame
    };
    const newState = ticTacToeReducer(initState, action);
    expect(newState).toEqual(expectedState);
  });

  it("init", () => {
    const action = {
      type: "INIT_DRAW"
    };

    const expectedState = initState;
    const newState = ticTacToeReducer(initState, action);
    expect(newState).toEqual(expectedState);
  });
});
