import { Color } from "../colors";

export class Player {
  _name: string;
  _color: Color;

  constructor(user: string, color: Color) {
    this._name = user;
    this._color = color;
  }

  clone(): Player {
    return new Player(this._name, this._color);
  }

  get name(): string {
    return this._name;
  }
  get color(): Color {
    return this._color;
  }
}
