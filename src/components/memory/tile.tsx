import * as React from "react";
import { connect } from "react-redux";
import "./styles.css";

interface TileProps {
  show: boolean;
  person: string;
}
interface TileState {
  show: boolean;
}

export class Tile extends React.Component<TileProps, TileState> {
  constructor(props: TileProps) {
    super(props);
    this.state = {
      show: this.props.show
    };
    this.switch = this.switch.bind(this);
  }

  renderImage() {
    return this.state.show ? <img className="tileImage" src={this.props.person + ".jpg"} alt="test" /> : <div />;
  }

  switch() {
    this.setState({ show: !this.state.show });
  }

  render() {
    return (
      <button className="tile" onClick={this.switch}>
        {this.renderImage()}
      </button>
    );
  }
}

interface StateFromProps {}

interface DispatchFromProps {}

const mapStateToProps = (state: any): StateFromProps => {
  return {};
};

const mapDispatchToProps = (dispatch: any): DispatchFromProps => {
  return {};
};

export default connect<StateFromProps, DispatchFromProps>(
  mapStateToProps,
  mapDispatchToProps
)(Tile);
