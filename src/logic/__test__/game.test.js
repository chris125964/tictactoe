import { Game, WIN_RATING, LOOSE_RATING } from "../game";
import { Player } from "../player";
import { Color } from "../../colors";
import { Move } from "../move";
import { PlayerList } from "../playerlist";

describe("Game", () => {
  let player1;
  let player2;
  let playerList;
  let game;
  let nrRows = 3;
  let nrCols = 3;

  beforeEach(() => {
    player1 = new Player("player1", Color.RED);
    player2 = new Player("player2", Color.GREEN);
    playerList = new PlayerList([player1, player2]);
    game = new Game(nrRows, nrCols, playerList, "x");
  });

  it("getter", () => {
    expect(game.Row).toBe(3);
    expect(game.Col).toBe(3);
    expect(game.Players).toBe(playerList);
  });

  it("clone", () => {
    let clonedGame = game.clone();
    expect(clonedGame.Row).toBe(3);
    expect(clonedGame.Col).toBe(3);
    expect(clonedGame.Players).toEqual(playerList);
  });

  it("hasGameWon", () => {
    expect(game.hasGameWon(player1)).toBe(false);
  });

  xit("display", () => {
    expect(game.display("")).toBe(undefined);
  });
  it("getRandomMove", () => {
    let randomMove = game.getRandomMove();
    expect(randomMove.Row).toBeGreaterThanOrEqual(0);
    expect(randomMove.Row).toBeLessThan(nrRows);
    expect(randomMove.Col).toBeGreaterThanOrEqual(0);
    expect(randomMove.Col).toBeLessThan(nrCols);
  });
  it("complete TicTacToe game", () => {
    let playerList = new PlayerList([player1, player2]);
    let tictactoe = new Game(3, 3, playerList, "x");
    let nextPlayer;
    // expect(tictactoe.whoseTurnIsIt()).toBe(player1);

    expect(tictactoe.getAllPossibleMoves().length).toBe(9);
    // player 1
    tictactoe.makeMove(new Move(0, 0));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player2);
    expect(tictactoe.hasGameWon(player1)).toBe(false);
    expect(tictactoe.whoseTurnIsIt()).toBe(player2);

    expect(tictactoe.getAllPossibleMoves().length).toBe(8);
    // player 2
    // let draws = tictactoe.getAllPossibleMoves();
    // draws.forEach(draw => console.log(`draw [${draw.row}, ${draw.col}]`));
    tictactoe.makeMove(new Move(1, 0));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player1);
    expect(tictactoe.whoseTurnIsIt()).toBe(player1);

    // player 1
    expect(tictactoe.getAllPossibleMoves().length).toBe(7);
    tictactoe.makeMove(new Move(0, 1));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player2);
    expect(tictactoe.hasGameWon(player1)).toBe(false);
    expect(tictactoe.whoseTurnIsIt()).toBe(player2);

    // player 2
    expect(tictactoe.getAllPossibleMoves().length).toBe(6);
    tictactoe.makeMove(new Move(1, 1));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player1);
    expect(tictactoe.whoseTurnIsIt()).toBe(player1);

    // player 1
    expect(tictactoe.getAllPossibleMoves().length).toBe(5);
    tictactoe.makeMove(new Move(0, 2));
    expect(tictactoe.hasGameWon(player1)).toBe(true);
  });

  it("artificial intelligence", () => {
    let playerList = new PlayerList([player1, player2]);
    let tictactoe = new Game(3, 3, playerList, "x");
    let move;
    let nextPlayer;

    //
    //  - - -
    //  - - -
    //  - - -

    // now player 1 -----------------------------------------------------------
    tictactoe.makeMove(new Move(0, 0));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 - -
    //  - - -
    //  - - -

    // now player 2 -----------------------------------------------------------
    tictactoe.makeMove(new Move(0, 1));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 -
    //  - - -
    //  - - -

    // now player 1 -----------------------------------------------------------
    tictactoe.makeMove(new Move(1, 2));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 -
    //  - - 1
    //  - - -

    // now player 2 -----------------------------------------------------------
    tictactoe.makeMove(new Move(0, 2));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 2
    //  - - 1
    //  - - -

    // now player 1 -----------------------------------------------------------
    tictactoe.makeMove(new Move(2, 2));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 2
    //  - - 1
    //  - - 1

    // now player 2 -----------------------------------------------------------
    expect(nextPlayer).toBe(player2);
    move = tictactoe.bestMove(player2, 1);
    expect(move.Row).toBe(1);
    expect(move.Col).toBe(1);
    expect(move.Rating).toBe(WIN_RATING);

    tictactoe.makeMove(new Move(1, 1));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 2
    //  - 2 1
    //  - - 1

    // now player 1 -----------------------------------------------------------
    move = tictactoe.bestMove(player1, 0);
    expect(move.Rating).toBe(LOOSE_RATING);

    tictactoe.makeMove(new Move(2, 0));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 2
    //  - 2 1
    //  1 - 1
    //

    // now player 2 -----------------------------------------------------------
    move = tictactoe.bestMove(player2, 0);
    expect(move.Rating).toBe(WIN_RATING);
    expect(move.Row).toBe(2);
    expect(move.Col).toBe(1);
    expect(move.Finished).toBe(true);

    tictactoe.makeMove(new Move(1, 0));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 2
    //  2 2 1
    //  1 - 1
    //

    // now player 1 -----------------------------------------------------------
    move = tictactoe.bestMove(player1, 1);
    expect(move.Rating).toBe(WIN_RATING);
    expect(move.Row).toBe(2);
    expect(move.Col).toBe(1);
    expect(move.Finished).toBe(true);

    tictactoe.makeMove(new Move(2, 1));
    nextPlayer = tictactoe.switchTurn();
    //
    //  1 2 2
    //  2 2 1
    //  1 1 1
    //

    // now player 1 -----------------------------------------------------------
    move = tictactoe.bestMove(player1, 1);
    expect(move.Finished).toBe(true);

    expect(tictactoe.hasGameWon(player1)).toBe(true);
  });

  it("artificial intelligence 2", () => {
    let playerList = new PlayerList([player1, player2]);
    let tictactoe = new Game(3, 3, playerList, "x");
    let move;
    let nextPlayer;

    // now player 1
    tictactoe.makeMove(new Move(0, 0));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player2);
    //    1 - -
    //    - - -
    //    - - -

    // now player 2
    tictactoe.makeMove(new Move(0, 1));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player1);
    //    1 2 -
    //    - - -
    //    - - -

    // now player 1
    move = tictactoe.bestMove(player1, 0, true);
    expect(move.Rating).toBe(WIN_RATING);
    expect(move.Row).toBe(1);
    expect(move.Col).toBe(0);

    tictactoe.makeMove(new Move(move.Row, move.Col));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player2);
    //    1 2 -
    //    1 - -
    //    - - -

    // now player 2
    tictactoe.makeMove(new Move(2, 0));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player1);
    //    1 2 -
    //    1 - -
    //    2 - -

    // now player 1
    move = tictactoe.bestMove(player1, 0, true);
    expect(move.Rating).toBe(WIN_RATING);
    expect(move.Row).toBe(1);
    expect(move.Col).toBe(1);

    tictactoe.makeMove(new Move(move.Row, move.Col));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player2);
    //    1 2 -
    //    1 1 -
    //    2 - -

    // now player 2
    tictactoe.makeMove(new Move(1, 2));
    nextPlayer = tictactoe.switchTurn();
    expect(nextPlayer).toBe(player1);
    //    1 2 -
    //    1 1 2
    //    2 - -

    // now player1
    move = tictactoe.bestMove(player1, 0);
    expect(move.Rating).toBe(WIN_RATING);
    expect(move.Row).toBe(2);
    expect(move.Col).toBe(2);
    // expect(move.Move.Rating).toBe(1.0);
    //    1 2 -
    //    1 1 2
    //    2 - 1
  });
});
