import { combineEpics } from "redux-observable";

import { countEpic } from "./epics";

export default combineEpics(countEpic);
