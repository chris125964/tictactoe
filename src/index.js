import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./index.css";
import configureStore from "./redux/configureStore";
import { Link, Route, BrowserRouter as Router } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import TicTacToe from "./components/TicTacToe";
import Memory from "./components/memory/Memory";

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div style={{ display: "flex" }}>
        <div className="sidebar">
          <ul>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/tictactoe">TicTacToe</Link>
            </li>
            <li>
              <Link to="/memory">Memory</Link>
            </li>
          </ul>
        </div>
        {/* <Router history={browserHistory}> */}
        <div className="mainWindow">
          <Route
            path="/about"
            component={() => {
              return <div>About!</div>;
            }}
          />
          <Route path="/tictactoe" component={() => <TicTacToe />} />
          <Route
            path="/memory"
            component={() => (
              <Memory
                order={[
                  "Jacob",
                  "Christine",
                  "Clemens",
                  "Theo",
                  "Matthias",
                  "Sibylle",
                  "Philipp",
                  "Nina",
                  "Benjamin",
                  "Leander",
                  "Wolfgang",
                  "Angelika"
                ]}
              />
            )}
          />
        </div>
        {/* </Router> */}
      </div>
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
