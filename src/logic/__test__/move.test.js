import { Move } from "../move";

describe("Row", () => {
  it("row - getter/setter", () => {
    let move = new Move();
    move.Row = 4;

    expect(move.Row).toBe(4);
  });

  it("col - getter/setter", () => {
    let drw = new Move();
    drw.Col = 5;

    expect(drw.Col).toBe(5);
  });
  it("rating - getter/setter", () => {
    let move = new Move();
    move.Rating = 0.6;

    expect(move.Rating).toBe(0.6);
  });
});
