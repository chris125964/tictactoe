import _ from "lodash";
import { Move } from "./move";

export class Moves {
  _moves: Move[];
  _maximize: boolean;

  constructor(maximize: boolean) {
    this._moves = [];
    this._maximize = maximize;
  }

  add(move: Move): void {
    this._moves.push(move);
  }

  // get Maximize(): boolean {
  //   return this._maximize;
  // }

  summarizeBestMove(): Move | undefined {
    let bestMove: Move | undefined;
    if (this._maximize) {
      bestMove = _.maxBy(this._moves, move => move.WholeRating + (move.Finished ? 1 : 0));
    } else {
      bestMove = _.minBy(this._moves, move => move.Rating + (move.Finished ? -1 : 0));
    }
    return bestMove;
  }

  display(moveNr: number): void {
    let prefix: string = "";
    let loop: number;
    for (loop = 0; loop < moveNr; loop++) {
      prefix += "  ";
    }
    let summary: string = "";
    this._moves.forEach((move, index) => {
      summary += `${index !== 0 ? " || " : ""} [${move.Row},${move.Col}]->${move.Rating} ${move.Finished ? "F" : ""}`;
      //console.log(`-> ${index}: [${move.Row}, ${move.Col}] -> ${move.Rating}`);
    });
    console.log(`${prefix}${moveNr})   ${summary}`);
  }
}
