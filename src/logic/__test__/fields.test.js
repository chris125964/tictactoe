import { Fields } from "../fields";

describe("Fields", () => {
  let fields;
  let defaultObject = "x";

  beforeEach(() => {
    fields = new Fields(2, 3, defaultObject);
  });

  it("method get", () => {
    let specificRow = 1;
    let specificCol = 1;
    let field = fields.get(specificRow, specificCol);
    expect(field.row).toBe(specificRow);
    expect(field.col).toBe(specificCol);
    expect(field.obj).toBe(defaultObject);
  });

  it("method all", () => {
    let specificRow = 1;
    let specificCol = 2;
    let specificText = "correct";
    fields.set(specificRow, specificCol, specificText);
    fields.each(field => {
      if (field.row === specificRow && field.col === specificCol) {
        expect(field.obj).toBe(specificText);
      }
    });
  });
});
