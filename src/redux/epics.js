import { delay, mapTo } from "rxjs/operators";
import { ofType } from "redux-observable";

export const countEpic = (action$, state$) =>
  action$.pipe(
    // filter(action => action.type === "MAKE_DRAW"),
    ofType("MAKE_DRAW"),
    delay(2000),
    // map(() => pong())
    mapTo({ type: "REDRAW" })
  );
export default [countEpic];
