import { Game } from "../logic/game";
import { createAction } from "redux-starter-kit";

export interface TicTacToeState {
  thisGame: Game;
  nrMoves: number;
}

export const initGame = createAction("INIT_GAME");
export const makeMove = createAction("MAKE_DRAW");
export const reMove = createAction("REDRAW");
export const pong = createAction("PONG");
